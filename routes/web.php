<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('welcome');
});

Auth::routes([ 'register' => false ]);

Route::get('/home', 'HomeController@index')->name('home');

/* Settings */
Route::group(array('prefix' => 'settings', 'middleware' => 'auth'), function () {
	/* Users */
	Route::group(array('prefix' => 'users', 'middleware' => 'auth'), function () {
		/* Users */
		Route::group(array('prefix' => 'users','middleware' => 'auth'), function () {
	   	Route::get('/index', 'UserController@index')->name('settings.users.users.index');
	   	Route::get('/create', 'UserController@create')->name('settings.users.users.create');
	   	Route::get('/edit/{id}', 'UserController@edit')->name('settings.users.users.edit');
	   	Route::post('/store', 'UserController@store')->name('settings.users.users.store');
	   	Route::post('/delete', 'UserController@delete')->name('settings.users.users.delete');
		});

		/* Roles */
		Route::group(array('prefix' => 'roles', 'middleware' => 'auth'), function () {
	   	Route::get('/index', 'RolesController@index')->name('settings.users.roles.index');
	   	Route::post('/store', 'RolesController@store')->name('settings.users.roles.store');
	   	Route::post('/delete', 'RolesController@delete')->name('settings.users.roles.delete');
		});

		/* Permissions */
		Route::group(array('prefix' => 'permissions', 'middleware' => 'auth'), function () {
	   	Route::get('/index', 'PermissionsController@index')->name('settings.users.permissions.index');
	   	Route::post('/store', 'PermissionsController@store')->name('settings.users.permissions.store');
	   	Route::post('/delete', 'PermissionsController@delete')->name('settings.users.permissions.delete');
		});
	});

	/* Company Details */
	Route::group(array('prefix' => 'company', 'middleware' => 'auth'), function () {
   	Route::get('/index', 'CompanyController@index')->name('settings.company.index');
   	// Route::post('/store', 'PermissionsController@store')->name('settings.users.permissions.store');
   	// Route::post('/delete', 'PermissionsController@delete')->name('settings.users.permissions.delete');
	});

});