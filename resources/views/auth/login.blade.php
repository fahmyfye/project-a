<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<!-- BEGIN: Head-->
<head>
   @include('layouts.partials.header')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">

   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- Page Title -->
   <title>Dark Layout - Stack Responsive Bootstrap 4 Admin Template</title>

   <!-- Page Icons -->
   <link rel="apple-touch-icon" href="{{ asset('images/ico/dashboard-fav.png') }}">
   <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/dashboard-fav.png') }}">
   <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

   <!-- BEGIN: Vendor CSS-->
   <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/vendors.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/ui/prism.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/pickers/datetime/bootstrap-datetimepicker.css') }}">
   <!-- END: Vendor CSS-->

   <!-- BEGIN: Theme CSS-->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-extended.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('css/colors.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('css/components.min.css') }}">
   <!-- END: Theme CSS-->

   <!-- BEGIN: Page CSS-->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/core/menu/vertical-menu.min.css') }}">
   <link rel="stylesheet" type="text/css" href="{{ asset('css/core/colors/palette-gradient.min.css') }}">
   @yield('css')
   <!-- END: Page CSS-->

   <!-- BEGIN: Custom CSS-->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
   <!-- END: Custom CSS-->

   <!-- BEGIN: Content-->
   <div class="app-content content">
      <div class="content-wrapper">
         <div class="content-header row">
         </div>
         <div class="content-body">
            <section class="flexbox-container">
               <div class="col-12 d-flex align-items-center justify-content-center">
                  <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                     {{-- Error Message --}}
                     @if($errors->any())
                        <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                           @foreach ($errors->all() as $error)
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                              </button>
                              <strong class="alert-danger">{{ $error }}</strong>
                              <br>
                           @endforeach
                        </div>
                     @endif
                     
                     <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                           <div class="card-title text-center">
                              <img src="{{ asset('images/background/login/login_icon.jpg') }}" alt="branding logo" style="max-width:80%;">
                           </div>
                           <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Company's Name</span></h6>
                        </div>
                        <div class="card-content">
                           {{-- <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using Account Details</span></p> --}}
                           <div class="card-body">
                              <form class="form-horizontal" method="post" action="{{ route('login') }}" novalidate>
                                 @csrf

                                 <fieldset class="form-group position-relative has-icon-left">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Your Username" required>
                                    <div class="form-control-position">
                                       <i class="ft-user"></i>
                                    </div>
                                 </fieldset>
                                 <fieldset class="form-group position-relative has-icon-left">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                                    <div class="form-control-position">
                                       <i class="fa fa-key"></i>
                                    </div>
                                 </fieldset>
                                 <div class="form-group row">
                                    <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                       <fieldset>
                                          <input type="checkbox" id="remember-me" class="chk-remember">
                                          <label for="remember-me"> Remember Me</label>
                                       </fieldset>
                                    </div>
                                    <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a href="recover-password.html" class="card-link">Forgot Password?</a></div>
                                 </div>
                                 <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> Login</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

         </div>
      </div>
   </div>
   <!-- END: Content-->

   <!-- BEGIN: Footer-->
   <footer class="footer footer-static footer-dark navbar-border">
      @include('layouts.partials.footer')       
   </footer>
   <!-- END: Footer-->

   <!-- BEGIN: Vendor JS-->
   <script src="{{ asset('vendors/js/vendors.min.js') }}"></script>
   <!-- BEGIN Vendor JS-->

   <!-- BEGIN: Page Vendor JS-->
   <script src="{{ asset('vendors/js/ui/prism.min.js') }}"></script>
   <script src="{{ asset('vendors/js/forms/select2/select2.full.min.js') }}"></script>
   <script src="{{ asset('vendors/js/pickers/datetime/moment-with-locales.min.js') }}"></script>
   <script src="{{ asset('vendors/js/pickers/datetime/bootstrap-datetimepicker.min.js') }}"></script>
   @yield('vendorjs')
   <!-- END: Page Vendor JS-->

   <!-- BEGIN: Theme JS-->
   <script src="{{ asset('js/core/app-menu.min.js') }}"></script>
   <script src="{{ asset('js/core/app.min.js') }}"></script>
   <!-- END: Theme JS-->

   <!-- BEGIN: Page JS-->
   @yield('js')
   <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
