<!-- Metas -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="">
<meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="Fy3 Entreprise">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Page Title -->
<title>Dark Layout - Stack Responsive Bootstrap 4 Admin Template</title>

<!-- Page Icons -->
<link rel="apple-touch-icon" href="{{ asset('images/ico/dashboard-fav.png') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/dashboard-fav.png') }}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/ui/prism.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/pickers/datetime/bootstrap-datetimepicker.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-extended.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/colors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/components.min.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/core/menu/vertical-menu.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/core/colors/palette-gradient.min.css') }}">
@yield('css')
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<!-- END: Custom CSS-->