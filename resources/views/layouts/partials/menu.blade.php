<div id="mymenu" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
   <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         <li class=" navigation-header">
            <span>Dashboard</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Dashboard"></i>
         </li>
         <li class=" nav-item">
            <a href="index.html">
               <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
               <span class="badge badge badge-primary badge-pill float-right mr-2"></span>
            </a>
            <ul class="menu-content">
               <li><a class="menu-item" href="dashboard-ecommerce.html">eCommerce</a>
               </li>
               <li><a class="menu-item" href="dashboard-analytics.html">Analytics</a>
               </li>
               <li><a class="menu-item" href="dashboard-fitness.html">Fitness</a>
               </li>
            </ul>
         </li>

         <!-- BEGIN: Settings -->
         <li class=" nav-item"><a href="#"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span></a>
            <ul class="menu-content">
               <!-- BEGIN: Users -->
               <li>
                  <a class="menu-item" href="#">Users</a>
                  <ul class="menu-content">
                     <li class="{{ request()->routeIs('settings.users.users.index') ? 'active' : '' }}">
                        <a class="menu-item" href="{!! route('settings.users.users.index') !!}">Users</a>
                     </li>
                     <li class="{{ request()->routeIs('settings.users.roles.index') ? 'active' : '' }}">
                        <a class="menu-item" href="{!! route('settings.users.roles.index') !!}">Roles</a>
                     </li>
                     <li class="{{ request()->routeIs('settings.users.permissions.index') ? 'active' : '' }}">
                        <a class="menu-item" href="{!! route('settings.users.permissions.index') !!}">Permissions</a>
                     </li>
                  </ul>
               </li>
               <!-- END: Users -->
            </ul>
         </li>
         
      </ul>
   </div>
</div>