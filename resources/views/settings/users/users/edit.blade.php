@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">New User</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item">Users</li>
                     <li class="breadcrumb-item active"><a href="{{ route('settings.users.users.index') }}">Users</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
               <a href="{{ route('settings.users.users.index') }}">
                  <button type="button" class="btn btn-outline-secondary btn-min-width mr-1 mb-1"><i class="ft-chevron-left"></i> Cancel</button>
               </a>
               <button id="save" type="button" class="btn btn-primary btn-min-width mr-1 mb-1"><i class="ft-save"></i> Save</button>
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
               <br>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <!-- Roles Listing -->
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                     <div class="card-body">
                        <div class="card-text">
                        </div>
                        <form id="form" class="form" method="post" action="{{ route('settings.users.users.store') }}">
                           {{ csrf_field() }}

                           <div class="form-body">
                              <h4 class="form-section"><i class="ft-user"></i> Login Details</h4>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="username">Username <small>( will be use as user login )</small></label>
                                       <input type="text" id="username" class="form-control" placeholder="Username" name="username" value="{!! $data->username !!}">
                                       <input type="hidden" name="id" value="{!! $data->id !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="password">Password</label>
                                       <input type="password" id="password" class="form-control" placeholder="Password" name="password">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="email">E-mail </label>
                                       <input type="email" id="email" class="form-control" placeholder="E-mail" name="email" value="{!! $data->email !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="form-group">
                                          <label for="staff_id">Staff ID</label>
                                          <input type="text" id="staff_id" class="form-control" placeholder="Staff ID" name="staff_id" value="{!! $data->staffid !!}">
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="role">Assign Role</label>
                                       <select id="role" class="form-control" name="role">
                                          @if(count($roles) > 0)
                                             <option value="" disabled>Select User Role</option>
                                             @foreach($roles as $value)
                                                <option value="{!! $value !!}" <?php echo ($data->roles->first()->name == $value) ? 'selected' : '' ?>>
                                                   {!! $value !!}
                                                </option>
                                             @endforeach
                                          @endif
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="form-group">
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="name">Name</label>
                                       <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{!! $data->name !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="nric">NRIC / Passport No.</label>
                                       <input type="text" id="nric" class="form-control" placeholder="NRIC / Passport No." name="nric" value="{!! $data->nric !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="address1">Address</label>
                                       <input type="text" id="address1" class="form-control" placeholder="Address" name="address1" value="{!! $data->address1 !!}">
                                    </div>
                                    <div class="form-group">
                                       <input type="text" id="address2" class="form-control" placeholder="Address" name="address2" value="{!! $data->address2 !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="phone">Contact No.</label>
                                       <input type="text" id="phone" class="form-control" placeholder="Phone" name="phone" value="{!! $data->phone !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="city">City</label>
                                       <input type="text" id="city" class="form-control" placeholder="City" name="city" value="{!! $data->city !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="postcode">Postcode</label>
                                       <input type="text" id="postcode" class="form-control" placeholder="Postcode" name="postcode" value="{!! $data->postcode !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="state">State</label>
                                       <input type="text" id="state" class="form-control" placeholder="State" name="state" value="{!! $data->state !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="country">Country</label>
                                       <input type="text" id="country" class="form-control" placeholder="Country" name="country" value="{!! $data->country !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="dob">Date of Birth</label>
                                       <input type="text" id="dob" class="form-control" placeholder="Date of Birth" name="dob" value="{!! $data->dob !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="marital">Marital Status</label>
                                       <input type="text" id="marital" class="form-control" placeholder="Marital Status" name="marital" value="{!! $data->marital !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="spouse_name">Spouse Name</label>
                                       <input type="text" id="spouse_name" class="form-control" placeholder="Spouse Name" name="spouse_name" value="{!! $data->spouse_name !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="spouse_phone">Spouse Phone No.</label>
                                       <input type="text" id="spouse_phone" class="form-control" placeholder="Spouse Phone No." name="spouse_phone" value="{!! $data->spouse_phone !!}">
                                    </div>
                                 </div>
                              </div>

                              <h4 class="form-section"><i class="fa fa-paperclip"></i> Company's Requirement</h4>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="bank">Bank Name</label>
                                       <input type="text" id="bank" class="form-control" placeholder="Bank Name" name="bank" value="{!! $data->bank !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="account">Account No.</label>
                                       <input type="text" id="account" class="form-control" placeholder="Account No." name="account" value="{!! $data->account !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="epf">EPF No.</label>
                                       <input type="text" id="epf" class="form-control" placeholder="EPF No." name="epf" value="{!! $data->epf !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="socso">SOCSO No.</label>
                                       <input type="text" id="socso" class="form-control" placeholder="SOCSO No." name="socso" value="{!! $data->socso !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="income_tax">Income Tax No.</label>
                                       <input type="text" id="income_tax" class="form-control" placeholder="Income Tax No." name="income_tax" value="{!! $data->income_tax !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                    </div>
                                 </div>
                              </div>

                              <h4 class="form-section"><i class="fa fa-paperclip"></i> Emergency Contact</h4>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="emergency_name">Name</label>
                                       <input type="text" id="emergency_name" class="form-control" placeholder="Name" name="emergency_name" value="{!! $data->emergency_name !!}">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="emergency_phone">Contact No.</label>
                                       <input type="text" id="emergency_phone" class="form-control" placeholder="Contact No." name="emergency_phone" value="{!! $data->emergency_phone !!}">
                                    </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="emergency_relationship">Relationship</label>
                                       <input type="text" id="emergency_relationship" class="form-control" placeholder="Relationship" name="emergency_relationship" value="{!! $data->emergency_relationship !!}">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/ Roles Listing -->
      </div>
   </div>
</div>
@endsection

@section('vendorjs')
   <!-- BEGIN: Datatables JS -->
   <script src="{{ asset('vendors/js/tables/datatables/datatables.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/datatables/dataTables.buttons.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.flash.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/jszip.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/pdfmake.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/vfs_fonts.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.html5.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.print.min.js') }}"></script>
   <!-- END: Datatables JS -->
@endsection

@section('js')
   <!-- BEGIN: Datatables JS -->
   <script src="{{ asset('js/scripts/tables/datatables/datatable-advanced.min.js') }}"></script>
   <!-- END: Datatables JS -->

   <!-- BEGIN: In-Page JS -->
   <script>
      $('#save').click(function() {
         $('#form').submit()
      })

      $('#delete').click(function() {
         $('#deleteform').submit()
      })

      // Mapping DataTables
      $('#table').DataTable({
         'scrollY'     : '50vh',
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : false,
         'info'        : true,
      })

      $('#dob').datetimepicker({
         format: 'YYYY-MM-DD'
      })
   </script>
   <!-- END: In-Page JS -->
@endsection