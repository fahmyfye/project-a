@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">User List</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item">Users</li>
                     <li class="breadcrumb-item active"><a href="{{ route('settings.users.users.index') }}">Users</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
               {{-- <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-bell"></i></button> --}}
               {{-- <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-life-buoy"></i></button> --}}
               {{-- <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-plus"></i></button> --}}
               <a href="{{ route('settings.users.users.create') }}">
                  <button type="button" class="btn btn-outline-secondary btn-min-width mr-1 mb-1"><i class="ft-plus"></i> New User</button>
               </a>
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <!-- Roles Listing -->
         <section id="css-classes" class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                     <div class="table-responsive">
                        @if (count($data) > 0)
                           <table id="tableusers." class="table table-hover table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th class="text-center" width="1%">#</th>
                                    <th>Name</th>
                                    <th class="text-center" width="10%">Role</th>
                                    <th class="text-center" width="5%">Status</th>
                                    <th class="text-center" width="15%">Created At</th>
                                    <th class="text-center" width="15%">Updated At</th>
                                    <th width="10%"></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($data as $key => $value)
                                    <tr>
                                       <td class="text-center">{!! ($key+1) !!}</td>
                                       <td>{!! $value->name !!}</td>
                                       <td class="text-center">{!! $value->roles->first()->name !!}</td>
                                       <td class="text-center">{!! $value->is_active !!}</td>
                                       <td class="text-center">{!! $value->created_at !!}</td>
                                       <td class="text-center">{!! $value->updated_at !!}</td>
                                       <td class="text-center">
                                          <div class="btn-group" role="group" aria-label="Basic example">
                                             <a href="{{ route('settings.users.users.edit', $value->id) }}" class="btn btn-primary"><i class="ft-edit-2"></i></a>
                                             <form id="deleteform" class="form form-horizontal"method="post" action="{{ route('settings.users.users.delete') }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{!! $value->id !!}">
                                                <button id="delete" class="btn btn-danger" type="button"><i class="ft-x"></i></button>
                                             </form>
                                          </div>
                                       </td>
                                    </tr>
                                 @endforeach
                              </tbody>
                           </table>

                        @else
                           <div class="alert alert-warning">Please set roles</div>
                           
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/ Roles Listing -->
      </div>
   </div>
</div>
@endsection

@section('vendorjs')
   <!-- BEGIN: Datatables JS -->
   <script src="{{ asset('vendors/js/tables/datatables/datatables.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/datatables/dataTables.buttons.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.flash.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/jszip.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/pdfmake.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/vfs_fonts.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.html5.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/buttons.print.min.js') }}"></script>
   <!-- END: Datatables JS -->
@endsection

@section('js')
   <!-- BEGIN: Datatables JS -->
   <script src="{{ asset('js/scripts/tables/datatables/datatable-advanced.min.js') }}"></script>
   <!-- END: Datatables JS -->

   <!-- BEGIN: In-Page JS -->
   <script>
      $('#save').click(function() {
         $('#form').submit()
      })

      $('#delete').click(function() {
         $('#deleteform').submit()
      })

      // Mapping DataTables
      $('#table').DataTable({
         'scrollY'     : '50vh',
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : false,
         'info'        : true,
      });
   </script>
   <!-- END: In-Page JS -->
@endsection