<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Dark Layout</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="index.html">Home</a>
                     </li>
                     <li class="breadcrumb-item"><a href="#">Page Layouts</a>
                     </li>
                     <li class="breadcrumb-item active">Dark Layout
                     </li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
               <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-bell"></i></button>
               <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-life-buoy"></i></button>
               <button class="btn-icon btn btn-secondary btn-round" type="button"><i class="ft-settings"></i></button>
            </div>
         </div>
      </div>
      <div class="content-body">
         <!-- Description -->
         <section id="description" class="card">
            <div class="card-header">
               <h4 class="card-title">Description</h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                     <p>The dark layout has a dark color navbar, navigation menu and footer. In this page you can experience it. If you want to create a dark version template you can use this layout.</p>
                  </div>
               </div>
            </div>
         </section>
         <!--/ Description -->
         <!-- CSS Classes -->
         <section id="css-classes" class="card">
            <div class="card-header">
               <h4 class="card-title">CSS Classes</h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                     <p>This table contains all classes related to the dark layout. This is a custom layout classes for dark layout page requirements.</p>
                     <p>All these options can be set via following classes:</p>
                     <div class="table-responsive">
                        <table class="table table-hover">
                           <thead>
                              <tr>
                                 <th>Classes</th>
                                 <th>Description</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <th scope="row"><code>.navbar-dark</code></th>
                                 <td>To set navbar dark color you need to add <code>.navbar-dark</code> class in navbar <code>&lt;nav&gt;</code> tag. Refer HTML markup line no 7.</td>
                              </tr>
                              <tr>
                                 <th scope="row"><code>.menu-dark</code></th>
                                 <td>To set the main navigation dark color on page <code>.menu-dark</code> class needs to add in navigation wrapper. Refer HTML markup line no 12.</td>
                              </tr>
                              <tr>
                                 <th scope="row"><code>.footer-dark</code></th>
                                 <td>To set the main footer dark color on page <code>.footer-dark</code> class needs to add in footer wrapper. Refer HTML markup line no 37.</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--/ CSS Classes -->
         <!-- HTML Markup -->
         <section id="html-markup" class="card">
            <div class="card-header">
               <h4 class="card-title">HTML Markup</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <ul class="list-inline mb-0">
                     <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                     <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                     <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
               </div>
            </div>
            <div class="card-content collapse show">
               <div class="card-body">
                  <div class="card-text">
                     <p>This section contains HTML Markup to create dark layout. This markup define where to add css classes to make navbar, navigation & footer dark.</p>
                     <ul>
                        <li><span class="text-bold-600">Line no 7:</span> Contain the <code>.navbar-dark</code> class for adjusting navbar color dark.</li>
                        <li><span class="text-bold-600">Line no 12:</span> Contain the <code>.menu-dark</code> class for adjusting main navigation color dark.</li>
                        <li><span class="text-bold-600">Line no 37:</span> Contain the <code>.footer-dark</code> class for adjusting footer color dark.</li>
                     </ul>
                     <p>Stack has a ready to use starter kit, you can use this layout directly by using the starter kit pages from the <code>stack-admin/starter-kit</code> folder.</p>
                     <pre data-line="7, 12, 37" class="language-markup">
                        <code class="language-markup">
                           &lt;!DOCTYPE html&gt;
                           &lt;html lang="en"&gt;
                           &lt;head&gt;&lt;/head&gt;
                           &lt;body data-menu="vertical-menu" class="vertical-layout vertical-menu 2-column menu-expanded"&gt;

                           &lt;!-- fixed-top--&gt;
                           &lt;nav role="navigation" class="header-navbar navbar-expand-sm navbar navbar-with-menu fixed-top navbar-dark navbar-shadow navbar-border"&gt;
                           ...
                           &lt;/nav&gt;

                           &lt;!-- BEGIN Navigation--&gt;
                           &lt;div class="main-menu menu-dark menu-fixed menu-shadow"&gt;
                           ...
                           &lt;/div&gt;
                           &lt;!-- END Navigation--&gt;

                           &lt;!-- BEGIN Content--&gt;
                           &lt;div class="content app-content"&gt;
                           &lt;div class="content-wrapper"&gt;
                           &lt;!-- content header--&gt;
                           &lt;div class="content-header row"&gt;
                           ...
                           &lt;/div&gt;
                           &lt;!-- content header--&gt;

                           &lt;!-- content right--&gt;
                           &lt;div class="content-right"&gt;
                           ...
                           &lt;/div&gt;
                           &lt;!-- content right--&gt;

                           &lt;/div&gt;
                           &lt;/div&gt;
                           &lt;!-- END Content--&gt;

                           &lt;!-- START FOOTER DARK--&gt;
                           &lt;footer class="footer footer-dark footer-fixed"&gt;
                           ...
                           &lt;/footer&gt;
                           &lt;!-- START FOOTER DARK--&gt;

                           &lt;/body&gt;
                           &lt;/html&gt;
                        </code>
                     </pre>
                  </div>
               </div>
            </div>
         </section>
         <!--/ HTML Markup -->
      </div>
   </div>
</div>