<?php

namespace App\Http\Models\Settings\Users;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
   use Notifiable, HasRoles, SoftDeletes;

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'name', 'username', 'email', 'password', 'is_active', 'created_by', 'updated_by'
   ];

   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
   protected $hidden = [
      'password', 'remember_token',
   ];

   /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
   protected $casts = [
      'email_verified_at' => 'datetime',
   ];

   /**
   * Get the profile associated with the user.
   */
   public function profile()
   {
      return $this->hasOne('App\Http\Models\Settings\Users\Profile', 'user_id', 'id');
   }
}
