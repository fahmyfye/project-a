<?php

namespace App\Http\Models\Settings\Users;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'user_id', 'staff_id', 'nric', 'phone', 'address1', 'address2', 'postcode', 'city', 'state', 'country', 'dob', 'marital', 'spouse_name', 'spouse_phone',
      'bank', 'account', 'epf', 'socso', 'income_tax', 'emergency_name', 'emergency_phone', 'emergency_relationship'
   ];

   /**
   * Get the user associated with the profile.
   */
   public function user()
   {
      return $this->belongsTo('App\Http\Models\Settings\Users\User', 'id', 'user_id');
   }
}
