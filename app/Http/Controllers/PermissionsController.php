<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Validator;

class PermissionsController extends Controller
{
   /**
   * Create a new controller instance.
   *
   * @return void
   */
   public function __construct()
   {
      $this->middleware('auth');
      // $this->middleware('role:Developer');
   }

	public function index()
   {
   	$data = Permission::all();
   	return view('settings.users.permissions.index', compact('data'));
   }

   public function store(Request $request)
   {
   	/* Validate Request */
   	$validator = $request->validate([
   		'name' => 'required|unique:roles|max:255'
   	]);

		/* Store Request */
		Permission::create(['name' => $request->name]);

		/* Flash Session Message*/
		session()->flash('message', 'Permission: '.$request->name.' Created');
		session()->flash('alert-class', 'alert-success');

		/* Redirect to Index*/
		return redirect()->route('settings.users.permissions.index');
   }

   public function delete(Request $request) {
   	$data = Permission::find($request->id);
   	$data->delete();

   	if($data) {
			/* Flash Session Message*/
			session()->flash('message', 'Permission: '.$data->name.' Deleted');
			session()->flash('alert-class', 'alert-warning');

			/* Redirect to Index*/
			return redirect()->route('settings.users.permissions.index');
   	}
   }
}
