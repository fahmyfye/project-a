<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Validator;

class RolesController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('auth');
		// $this->middleware('role:Developer');
	}

   public function index()
   {
   	$data = Role::all();
   	return view('settings.users.roles.index', compact('data'));
   }

   public function store(Request $request)
   {
   	/* Validate Request */
   	$validator = $request->validate([
   		'name' => 'required|unique:roles|max:255'
   	]);

		/* Store Request */
		Role::create(['name' => $request->name]);

		/* Flash Session Message*/
		session()->flash('message', 'Role: '.$request->name.' Created');
		session()->flash('alert-class', 'alert-success');

		/* Redirect to Index*/
		return redirect()->route('settings.users.roles.index');
   }

   public function delete(Request $request) {
   	$data = Role::find($request->id);
   	$data->delete();

   	if($data) {
			/* Flash Session Message*/
			session()->flash('message', 'Role: '.$data->name.' Deleted');
			session()->flash('alert-class', 'alert-warning');

			/* Redirect to Index*/
			return redirect()->route('settings.users.roles.index');
   	}
   }

}
