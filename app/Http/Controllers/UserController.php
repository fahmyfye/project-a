<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Settings\Users\User;
use App\Http\Models\Settings\Users\Profile;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Auth;
use Hash;

class UserController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:Developer|Super Admin');
	}

   public function index()
   {
   	$data = User::with('roles')->get();
   	// dd($data);
   	return view('settings.users.users.index', compact('data'));
   }

   public function create()
   {
   	if(Auth::user()->hasRole('Developer')) {
   		$role = Role::pluck('name');

   	} else if(Auth::user()->hasRole('Super Admin')) {
   		$role = Role::where('name', '!=', 'Developer')->where('name', '!=', 'Parents')->pluck('name');

   	} else {
   		$role = Role::where('name', '!=', 'Developer')->where('name', '!=', 'Super Admin')->where('name', '!=', 'Parents')->pluck('name');

   	}

   	return view('settings.users.users.create', compact('role'));
   }

   public function store(Request $request)
   {
   	/* Validate Request */
   	$validator = $request->validate([
			'name'                   => 'required|string|max:255',
			'email'                  => 'required|string|email|max:255|unique:users',
			'password'               => 'required|string|min:6',
			'staff_id'               => 'nullable|string|max:255',
			'nric'                   => 'required|alpha_dash|max:255',
			'phone'                  => 'required|string|max:255',
			'address1'               => 'required|string|max:255',
			'address2'               => 'nullable|string|max:255',
			'postcode'               => 'required|digits:5',
			'city'                   => 'required|string|max:255',
			'country'                => 'required|string|max:255',
			'dob'                    => 'required',
			'marital'                => 'required',
			'spouse_name'            => 'nullable|string|max:255',
			'spouse_phone'           => 'nullable|alpha_dash|max:255',
			'bank'                   => 'required|string|max:255',
			'account'                => 'required|alpha_dash|max:255',
			'epf'                    => 'nullable|digits_between:5,10',
			'socso'                  => 'nullable|alpha_dash|max:255',
			'emergency_name'         => 'required|string|max:255',
			'emergency_phone'        => 'required|alpha_dash|max:255',
			'emergency_relationship' => 'required|string|max:255',
   	]);

   	$user = User::create([
			'name'       => ucwords($request->name),
			'email'      => strtolower($request->email),
			'password'   => Hash::make($request->password),
			'created_by' => ucwords(Auth::user()->name),
		]);

   	if($user) {
   		$find = User::find($user->id);
	   	$find->assignRole($request->role);
   		   	
	   	$profile = Profile::create([
				'user_id'                => $user->id,
				'staff_id'               => $request->staff_id,
				'nric'                   => $request->nric,
				'phone'                  => $request->phone, 
				'address1'               => ucwords($request->address1), 
				'address2'               => ucwords($request->address2), 
				'postcode'               => $request->postcode,
				'city'                   => ucwords($request->city),
				'country'                => ucwords($request->country),
				'dob'                    => $request->dob,
				'marital'                => ucwords($request->marital), 
				'spouse_name'            => ucwords($request->spouse_name), 
				'spouse_phone'           => $request->spouse_phone,
				'bank'                   => ucwords($request->bank),
				'account'                => $request->account,
				'epf'                    => $request->epf,
				'socso'                  => $request->socso,
				'emergency_name'         => ucwords($request->emergency_name),
				'emergency_phone'        => $request->emergency_phone,
				'emergency_relationship' => ucwords($request->emergency_relationship),
   		]);

	      /* Return Response */
   		session()->flash('message', 'User Profile Saved');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('settings.users.users.index');

   	} else {
   		session()->flash('message', 'Fail to Save User Profile');
   		session()->flash('alert-class', 'alert-warning');
   	}

   }

   public function edit($id)
   {
   	$data = User::with('roles')->with('profile')->find($id);
   	$roles = Role::pluck('name');
   	return view('settings.users.users.edit', compact('data', 'roles'));
   }

   public function update(Request $request)
   {
   	/* Validate Request */
   	$validator = $request->validate([
			'name'                   => 'required|string|max:255',
			'email'                  => 'required|string|email|max:255|unique:users',
			'password'               => 'required|string|min:6',
			'staff_id'               => 'nullable|string|max:255',
			'nric'                   => 'required|alpha_dash|max:255',
			'phone'                  => 'required|string|max:255',
			'address1'               => 'required|string|max:255',
			'address2'               => 'nullable|string|max:255',
			'postcode'               => 'required|digits:5',
			'city'                   => 'required|string|max:255',
			'country'                => 'required|string|max:255',
			'dob'                    => 'required',
			'marital'                => 'required',
			'spouse_name'            => 'nullable|string|max:255',
			'spouse_phone'           => 'nullable|alpha_dash|max:255',
			'bank'                   => 'required|string|max:255',
			'account'                => 'required|alpha_dash|max:255',
			'epf'                    => 'nullable|digits_between:5,10',
			'socso'                  => 'nullable|alpha_dash|max:255',
			'emergency_name'         => 'required|string|max:255',
			'emergency_phone'        => 'required|alpha_dash|max:255',
			'emergency_relationship' => 'required|string|max:255',
   	]);

   	$user = User::find($request->id);
   	$user->assignRole($request->role);
   	$user->update([
   			'name'       => ucwords($request->name),
   			'email'      => strtolower($request->email),
   			'password'   => Hash::make($request->password),
   			'updated_by' => ucwords(Auth::user()->name),
   		]);

   	$profile = Profile::where('user_id', $request->id)->first();
   	$profile->update([
   			'staff_id'               => $request->staff_id,
   			'nric'                   => $request->nric,
   			'phone'                  => $request->phone, 
   			'address1'               => ucwords($request->address1), 
   			'address2'               => ucwords($request->address2), 
   			'postcode'               => $request->postcode,
   			'city'                   => ucwords($request->city),
   			'country'                => ucwords($request->country),
   			'dob'                    => $request->dob,
   			'marital'                => ucwords($request->marital), 
   			'spouse_name'            => ucwords($request->spouse_name), 
   			'spouse_phone'           => $request->spouse_phone,
   			'bank'                   => ucwords($request->bank),
   			'account'                => $request->account,
   			'epf'                    => $request->epf,
   			'socso'                  => $request->socso,
   			'emergency_name'         => ucwords($request->emergency_name),
   			'emergency_phone'        => $request->emergency_phone,
   			'emergency_relationship' => ucwords($request->emergency_relationship),
   		]);

      /* Return Response */
   	if($data) {
   		session()->flash('message', 'User Profile Saved');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('settings.users.users.index');

   	} else {
   		session()->flash('message', 'Fail to Save User Profile');
   		session()->flash('alert-class', 'alert-warning');
   	}
   }

   public function view($id)
   {
   	$data = User::with('roles')->with('profile')->find($id);
   	return view('settings.users.users.view', compact('data'));
   }

   public function delete(Request $request) {
   	$data = User::find($request->id);
   	$data->delete();

   	if($data) {
			/* Flash Session Message*/
			session()->flash('message', 'User: '.$data->name.' Deleted');
			session()->flash('alert-class', 'alert-warning');

			/* Redirect to Index*/
			return redirect()->route('settings.users.users.index');
   	}
   }
}
