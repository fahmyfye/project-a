<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('staff_id')->nullable();
            $table->string('nric');
            $table->string('phone');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('postcode');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->date('dob');
            $table->string('marital');
            $table->string('spouse_name')->nullable();
            $table->string('spouse_phone')->nullable();
            $table->string('bank');
            $table->string('account');
            $table->string('epf')->nullable();
            $table->string('socso')->nullable();
            $table->string('income_tax')->nullable();
            $table->string('emergency_name');
            $table->string('emergency_phone');
            $table->string('emergency_relationship');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
