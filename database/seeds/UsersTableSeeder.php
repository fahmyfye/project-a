<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
      	[
	      	'name' => 'Fahmy',
	      	'username' => 'fahmy',
	      	'email' => 'fahmy@fye.com',
	      	'password' => bcrypt('123456')
	      ],
      	[
	      	'name' => 'Adzreen',
	      	'username' => 'adzreen',
	      	'email' => 'adzreen@fye.com',
	      	'password' => bcrypt('123456')
	      ],
      	[
	      	'name' => 'Super Admin',
	      	'username' => 'superadmin',
	      	'email' => 'superadmin@fye.com',
	      	'password' => bcrypt('123456')
	      ],
      	[
	      	'name' => 'Admin',
	      	'username' => 'admin',
	      	'email' => 'admin@fye.com',
	      	'password' => bcrypt('123456')
	      ],
      	[
	      	'name' => 'Staff A',
	      	'username' => 'staffa',
	      	'email' => 'staffa@fye.com',
	      	'password' => bcrypt('123456')
	      ],
      	[
	      	'name' => 'Staff B',
	      	'username' => 'staffb',
	      	'email' => 'staffb@fye.com',
	      	'password' => bcrypt('123456')
	      ],
	   ]);
    }
}
