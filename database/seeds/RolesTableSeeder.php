<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		DB::table('roles')->insert([
      	[
	      	'name' => 'Developer',
	      	'guard_name' => 'web',
	      ],
      	[
	      	'name' => 'Super Admin',
	      	'guard_name' => 'web',
	      ],
      	[
	      	'name' => 'Admin',
	      	'guard_name' => 'web',
	      ],
      	[
	      	'name' => 'Staff',
	      	'guard_name' => 'web',
	      ],
      	[
	      	'name' => 'Parents',
	      	'guard_name' => 'web',
	      ],
	   ]);
	}
}
